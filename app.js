var myApp = angular.module('myApp', ['ngRoute']);
var employee ='';
myApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl:'Views/list.html',
            controller: 'empController'
        })
        .when('/employees', {
            templateUrl:'Views/list.html',
            controller: 'empController'
        })
        .when('/employees/create', {
            templateUrl:'Views/create.html',
            controller: 'empController'
        })
        .when('/employees/:id/show', {
            templateUrl:'Views/show.html',
            controller: 'empController'
        })
        .when('/employees/:id/edit', {
            templateUrl:'Views/edit.html',
            controller: 'empController'
        })
        .when('/employees/:id/delete', {
            templateUrl:'Views/delete.html',
            controller: 'empController'
        });
});